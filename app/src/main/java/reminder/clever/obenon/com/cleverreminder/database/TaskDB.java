package reminder.clever.obenon.com.cleverreminder.database;

public abstract class TaskDB {
    public final static String TABLE_NAME = "tasks";

    public enum Cols {
        UUID, TITLE, TIMESTAMP, SOLVED, PRIORITY
    }
}
