package reminder.clever.obenon.com.cleverreminder.tab_fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import reminder.clever.obenon.com.cleverreminder.R;
import reminder.clever.obenon.com.cleverreminder.adapter.TaskAdapter;
import reminder.clever.obenon.com.cleverreminder.dialog.CreateDialogFragment;
import reminder.clever.obenon.com.cleverreminder.model.Task;
import reminder.clever.obenon.com.cleverreminder.model.TaskLab;


public class CurrentFragment extends Fragment {
    private TaskLab mTaskLab;
    private FragmentManager mFragmentManager;
    public final static String CRETE_TASK_DIALOG = "create_dialog";
    private final static int CREATE_TASK_REQUEST = 3;
    private RecyclerView mCrimeRecyclerView;
    private TaskAdapter mTaskAdapter;
    private TextView mEmptyList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTaskLab = TaskLab.getTaskLab(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RelativeLayout rootView = (RelativeLayout) inflater.inflate(R.layout.current_tasks_fragment, container, false);
        mEmptyList = (TextView) rootView.findViewById(R.id.empty_task);
        mFragmentManager = getFragmentManager();
        mCrimeRecyclerView = (RecyclerView) rootView.findViewById(R.id.current_tasks_list_view);
        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        updateUI();

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateDialogFragment createDialogFragment = new CreateDialogFragment();
                createDialogFragment.setTargetFragment(CurrentFragment.this, CREATE_TASK_REQUEST);
                createDialogFragment.show(mFragmentManager, CRETE_TASK_DIALOG);
            }
        });
        return rootView;
    }

    private void updateUI() {
        List<Task> taskList = mTaskLab.getTasks();
        if (!taskList.isEmpty()) {
            mEmptyList.setVisibility(View.INVISIBLE);
            if (mTaskAdapter == null) {
                mTaskAdapter = new TaskAdapter(taskList);
                mCrimeRecyclerView.setAdapter(mTaskAdapter);
            } else {
                mTaskAdapter.updateData(taskList);
            }
        }
        else {
            mEmptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREATE_TASK_REQUEST){
            if (resultCode == Dialog.BUTTON_POSITIVE) {
                Task task = (Task) data.getSerializableExtra(CreateDialogFragment.TASK_ARG);
                mTaskLab.addTask(task);
                updateUI();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }
}
