package reminder.clever.obenon.com.cleverreminder.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import reminder.clever.obenon.com.cleverreminder.R;
import reminder.clever.obenon.com.cleverreminder.holder.TaskHolder;
import reminder.clever.obenon.com.cleverreminder.model.Task;


public class TaskAdapter extends RecyclerView.Adapter<TaskHolder> {
    private List<Task> mList;

    public TaskAdapter(List<Task> list) {
        mList = list;

    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.task_item_view, parent, false);
        return new TaskHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {
        Task task = mList.get(position);
        holder.bindTask(task);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateData(List<Task> taskList){
        mList.clear();
        mList = taskList;
        notifyDataSetChanged();
    }
}
