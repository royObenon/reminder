package reminder.clever.obenon.com.cleverreminder.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import reminder.clever.obenon.com.cleverreminder.R;
import reminder.clever.obenon.com.cleverreminder.model.Task;


public class TaskHolder extends RecyclerView.ViewHolder {
    TextView mTaskTitle, mTaskDate, mTaskTime;
    ImageView mTaskPriority;

    public TaskHolder(View itemView) {
        super(itemView);
        mTaskTitle = (TextView) itemView.findViewById(R.id.task_text);
        mTaskDate = (TextView) itemView.findViewById(R.id.task_date);
        mTaskTime = (TextView) itemView.findViewById(R.id.task_time);
        mTaskPriority = (ImageView) itemView.findViewById(R.id.task_priority);
    }

    public void bindTask(Task task){
        mTaskTitle.setText(task.getTitle());
        mTaskDate.setText(Task.dateToString(task.getDate()));
        mTaskTime.setText(Task.timeToString(task.getDate()));
        mTaskPriority.setImageResource(task.getPriority());
    }


}

