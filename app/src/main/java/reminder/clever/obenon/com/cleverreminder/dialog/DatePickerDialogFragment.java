package reminder.clever.obenon.com.cleverreminder.dialog;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

public class DatePickerDialogFragment extends DialogFragment {
    public static final String DATE_EXTRA = "date_picker";


    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        return new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);
                sendResult(Dialog.BUTTON_POSITIVE, calendar);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    private void sendResult(int resultOk, Calendar date) {
        if (getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(DATE_EXTRA, date);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultOk, intent);
    }

}
