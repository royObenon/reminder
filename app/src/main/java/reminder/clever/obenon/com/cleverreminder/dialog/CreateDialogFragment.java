package reminder.clever.obenon.com.cleverreminder.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.Calendar;
import java.util.UUID;

import reminder.clever.obenon.com.cleverreminder.R;
import reminder.clever.obenon.com.cleverreminder.model.Task;

import static reminder.clever.obenon.com.cleverreminder.R.id.date;
import static reminder.clever.obenon.com.cleverreminder.dialog.DatePickerDialogFragment.DATE_EXTRA;

@SuppressWarnings("ALL")
public class CreateDialogFragment extends DialogFragment {
    private TextInputLayout mTextInputLayout;
    private EditText mTitle, mDate, mTime;
    private RadioButton mHigh, mLow;
    private FragmentManager mFragmentManager;
    private static final String DATE_PICKER = "date_picker";
    private static final String TIME_PICKER = "time_picker";
    public static final String TASK_ARG = "task_arg";
    private static final int DATE_PICKER_REQUEST = 1;
    private static final int TIME_PICKER_REQUEST = 2;
    private AlertDialog mCreateDialog;
    private Calendar mTaskDate;
    private Button mCreateOkButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getFragmentManager();
        mTaskDate = Calendar.getInstance();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.create_task_fragment_v2, null);
        mTextInputLayout = (TextInputLayout) view.findViewById(R.id.text_input_title);
        mTextInputLayout.setError(getString(R.string.empty_name));

        mTitle = (EditText) view.findViewById(R.id.input_name);
        mDate = (EditText) view.findViewById(date);
        mTime = (EditText) view.findViewById(R.id.time);
        mHigh = (RadioButton) view.findViewById(R.id.high_priority);
        mLow = (RadioButton) view.findViewById(R.id.low_priority);

        mTextInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)){
                    mTextInputLayout.setError(getString(R.string.empty_name));
                    mCreateOkButton.setEnabled(false);
                }
                else {
                    mTextInputLayout.setError(null);
                    mCreateOkButton.setEnabled(true);
                }
            }
        });
        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();
                datePickerDialogFragment.setTargetFragment(CreateDialogFragment.this, DATE_PICKER_REQUEST);
                datePickerDialogFragment.show(mFragmentManager, DATE_PICKER);
            }
        });

        mTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timePickerDialogFragment = new TimePickerDialogFragment();
                timePickerDialogFragment.setTargetFragment(CreateDialogFragment.this, TIME_PICKER_REQUEST);
                timePickerDialogFragment.show(mFragmentManager, TIME_PICKER);
            }
        });

        mCreateDialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isDateInvalid()) {
                            Toast.makeText(getContext(), getString(R.string.create_verify), Toast.LENGTH_LONG).show();
                        }
                        else {
                            Task task = new Task(UUID.randomUUID(), mTaskDate, mTitle.getText().toString(), getPriority(), false);
                            sendResult(Dialog.BUTTON_POSITIVE, task);
                        }


                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();
        mCreateDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                mCreateOkButton = mCreateDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                mCreateOkButton.setEnabled(false);
            }
        });
        return mCreateDialog;
    }

    private boolean isDateInvalid() {
        Calendar currentDate = Calendar.getInstance();
        return mTaskDate.getTimeInMillis() <= currentDate.getTimeInMillis();
    }


    private int getPriority(){
        if (mHigh.isChecked()){
            return R.drawable.ic_high_priority;
        }
        return R.drawable.ic_low_priority;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DATE_PICKER_REQUEST) {
            if (resultCode == Dialog.BUTTON_POSITIVE) {
                Calendar result = (Calendar) data.getSerializableExtra(DATE_EXTRA);
                mTaskDate.set(result.get(Calendar.YEAR),
                        result.get(Calendar.MONTH),
                        result.get(Calendar.DAY_OF_MONTH),
                        12, 0);
                updateDate();
            }
        }
        if (requestCode == TIME_PICKER_REQUEST) {
            if (resultCode == Dialog.BUTTON_POSITIVE) {
                Calendar result = (Calendar) data.getSerializableExtra(TimePickerDialogFragment.TIME_EXTRA);
                mTaskDate.set(Calendar.HOUR_OF_DAY, result.get(Calendar.HOUR_OF_DAY));
                mTaskDate.set(Calendar.MINUTE, result.get(Calendar.MINUTE));
                mTaskDate.set(Calendar.SECOND, 0);
                updateTime();
            }
        }
    }

    private void updateTime() {
        mTime.setText(Task.timeToString(mTaskDate.getTimeInMillis()));
    }

    private void updateDate() {
        mDate.setText(Task.dateToString(mTaskDate.getTimeInMillis()));
    }

    private void sendResult(int resultOk, Task task){
        if (getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(TASK_ARG, task);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultOk, intent);
    }

}
