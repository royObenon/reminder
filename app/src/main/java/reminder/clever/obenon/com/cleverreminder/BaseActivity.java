package reminder.clever.obenon.com.cleverreminder;


import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ShareCompat.IntentBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import reminder.clever.obenon.com.cleverreminder.adapter.SectionsPagerAdapter;
import reminder.clever.obenon.com.cleverreminder.dialog.ExitDialogFragment;
import reminder.clever.obenon.com.cleverreminder.tab_fragment.CurrentFragment;
import reminder.clever.obenon.com.cleverreminder.tab_fragment.DoneFragment;
import reminder.clever.obenon.com.cleverreminder.util.ISelected;

@SuppressWarnings("ALL")
public class BaseActivity extends AppCompatActivity implements ISelected {
    public final static String EXIT_DIALOG = "exit_dialog";
    private FragmentManager mFragmentManager;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);
        getSupportActionBar().setElevation(0);
        mFragmentManager = getSupportFragmentManager();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.addFragment(new CurrentFragment(), getString(R.string.current_tab));
        mSectionsPagerAdapter.addFragment(new DoneFragment(), getString(R.string.done_tab));

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                Toast.makeText(this, "Current section in develop", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_exit:
                ExitDialogFragment exitDialog = new ExitDialogFragment();
                exitDialog.show(mFragmentManager, EXIT_DIALOG);
                return true;
            case R.id.comment_send:
                IntentBuilder intent = IntentBuilder.from(this);
                intent.addEmailTo(getString(R.string.email));
                intent.setSubject(getString(R.string.subject, getString(R.string.app_name)));
                intent.setType("message/rfc822");
                if(getPackageManager().resolveActivity(intent.getIntent(), PackageManager.MATCH_DEFAULT_ONLY) != null) {
                    startActivity(intent.getIntent());
                } else {
                    Snackbar.make(getCurrentFocus(), getString(R.string.not_found_email_app), Snackbar.LENGTH_LONG).show();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void close(){
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            finishAndRemoveTask();
        } else {
            finish();
        }
        Toast.makeText(this, getString(R.string.app_name) + " " + getString(R.string.closed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSelectedButton(int resultOk) {
        if (resultOk == Dialog.BUTTON_POSITIVE){
            close();
        }
    }
}
