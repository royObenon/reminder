package reminder.clever.obenon.com.cleverreminder.model;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

public class Task implements Serializable {

    private UUID mUUID;
    private Calendar mDate;
    private String mTitle;
    private int mPriority;
    private boolean mSolved;

    public Task(UUID uuid,Calendar date, String title, int priority, boolean solved) {
        mUUID = uuid;
        mDate = date;
        mTitle = title;
        mPriority = priority;
        mSolved = solved;
    }

    public int getPriority(){
        return mPriority;
    }

    public long getDate() {
        return mDate.getTimeInMillis();
    }

    public String getTitle() {
        return mTitle;
    }

    public UUID getUUID() {
        return mUUID;
    }

    public boolean isSolved(){
        return mSolved;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return mDate.equals(task.mDate) && (mTitle != null ? mTitle.equals(task.mTitle) : task.mTitle == null);

    }

    @Override
    public int hashCode() {
        int result = mUUID.hashCode();
        result = 31 * result + mDate.hashCode();
        result = 31 * result + mTitle.hashCode();
        result = 31 * result + mPriority;
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                ", mUUID=" + mUUID +
                ", mDate=" + mDate +
                ", mTitle='" + mTitle + '\'' +
                ", mPriority=" + mPriority +
                ", mSolved=" + mSolved +
                '}';
    }

    public static String timeToString(long date){
        Calendar calendar = longToDate(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String dateToString(long date){
        Calendar calendar = longToDate(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Calendar longToDate(long timestamp){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return calendar;
    }

    public static void main(String[] args) throws InterruptedException {

    }
}
