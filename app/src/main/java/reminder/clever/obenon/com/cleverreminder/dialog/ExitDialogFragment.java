package reminder.clever.obenon.com.cleverreminder.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import reminder.clever.obenon.com.cleverreminder.R;
import reminder.clever.obenon.com.cleverreminder.util.ISelected;


public class ExitDialogFragment extends DialogFragment {
    private ISelected callBack;
    private final static String EXIT_DIALOG_TAG = ExitDialogFragment.class.getSimpleName();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            callBack = (ISelected) context;
        }catch (ClassCastException e) {
            Log.d(EXIT_DIALOG_TAG, "Activity doesn't implement the ISelectedData interface");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.exit_message))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.onSelectedButton(Dialog.BUTTON_POSITIVE);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();
    }

}
