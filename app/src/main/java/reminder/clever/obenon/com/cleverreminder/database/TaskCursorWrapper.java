package reminder.clever.obenon.com.cleverreminder.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.UUID;

import reminder.clever.obenon.com.cleverreminder.model.Task;


public class TaskCursorWrapper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public TaskCursorWrapper(Cursor cursor) {
        super(cursor);
    }


    public Task getTask(){
        String uuid = getString(getColumnIndex(TaskDB.Cols.UUID.toString()));
        long timestamp = getLong(getColumnIndex(TaskDB.Cols.TIMESTAMP.toString()));
        String title = getString(getColumnIndex(TaskDB.Cols.TITLE.toString()));
        boolean solved = Boolean.getBoolean(getString(getColumnIndex(TaskDB.Cols.SOLVED.toString())));
        int priority = getInt(getColumnIndex(TaskDB.Cols.PRIORITY.toString()));
        return new Task(UUID.fromString(uuid), Task.longToDate(timestamp), title, priority, solved);
    }
}
