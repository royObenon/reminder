package reminder.clever.obenon.com.cleverreminder.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import reminder.clever.obenon.com.cleverreminder.database.TaskDB.Cols;

public class TaskSQLiteHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DB_NAME = "taskBase.db";


    public TaskSQLiteHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+ TaskDB.TABLE_NAME + "(" +
        "id integer primary key autoincrement, " +
        Cols.UUID + "  not null, " +
        Cols.TITLE + " not null, " +
        Cols.TIMESTAMP + " not null, " +
        Cols.SOLVED + " not null, " +
        Cols.PRIORITY + " not null " + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
