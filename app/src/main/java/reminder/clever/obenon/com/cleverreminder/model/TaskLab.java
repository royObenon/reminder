package reminder.clever.obenon.com.cleverreminder.model;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import reminder.clever.obenon.com.cleverreminder.database.TaskCursorWrapper;
import reminder.clever.obenon.com.cleverreminder.database.TaskDB;
import reminder.clever.obenon.com.cleverreminder.database.TaskDB.Cols;
import reminder.clever.obenon.com.cleverreminder.database.TaskSQLiteHelper;

@SuppressWarnings("ALL")
public class TaskLab {
    public static TaskLab sTaskLab;
    private Context mContext;
    private SQLiteDatabase mSQLiteDatabase;

    public static TaskLab getTaskLab(Context context){
        if (sTaskLab == null){
            sTaskLab = new TaskLab(context);
        }
        return sTaskLab;
    }
    private TaskLab(Context context){
        mContext = context;
        mSQLiteDatabase = new TaskSQLiteHelper(mContext).getWritableDatabase();
    }

    public List<Task> getTasks(){
        List<Task> taskList = new ArrayList<>();
        TaskCursorWrapper taskCursorWrapper = queryCrimes(null, null);
        try {
            taskCursorWrapper.moveToFirst();
            while (!taskCursorWrapper.isAfterLast()) {
                taskList.add(taskCursorWrapper.getTask());
                taskCursorWrapper.moveToNext();
            }
        }
        finally {
            taskCursorWrapper.close();
        }
        return taskList;
    }

    public Task getTask(UUID uuid){
        TaskCursorWrapper taskCursorWrapper = queryCrimes(
                Cols.UUID + " = ?",
                new String[]{uuid.toString()}
        );
        try {
            if (taskCursorWrapper.getCount() == 0){
                return null;
            }
            taskCursorWrapper.moveToFirst();
            return taskCursorWrapper.getTask();
        } finally {
            taskCursorWrapper.close();
        }
    }

    public void addTask(Task task){
        ContentValues contentValues = getContentValues(task);
        mSQLiteDatabase.insert(TaskDB.TABLE_NAME, null, contentValues);
    }

    private static ContentValues getContentValues(Task task){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Cols.UUID.toString(), task.getUUID().toString());
        contentValues.put(Cols.TITLE.toString(), task.getTitle());
        contentValues.put(Cols.TIMESTAMP.toString(), task.getDate());
        contentValues.put(Cols.PRIORITY.toString(), task.getPriority());
        contentValues.put(Cols.SOLVED.toString(), task.isSolved());
        return contentValues;
    }

    public void updateTask(Task task){
        String uuid = task.getUUID().toString();
        ContentValues contentValues = getContentValues(task);
        mSQLiteDatabase.update(TaskDB.TABLE_NAME, contentValues,
                Cols.UUID + " =?",
                new String[]{uuid});
    }

    private TaskCursorWrapper queryCrimes(String whereClause, String[] whereArgs){
        Cursor cursor = mSQLiteDatabase.query(
                TaskDB.TABLE_NAME,
                null,
                whereClause,
                whereArgs, null, null, null);
        return new TaskCursorWrapper(cursor);
    }
}
